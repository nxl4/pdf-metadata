# PDF Metadata

## General Description

PDF Metadata is an automated utility that extracts metadata from PDF file. The utility is used by passing arguments to the script from the command line. As this utility is intended for digital forensics use case, in distinction from PDF parsing libraries (e.g. PyPDF2), which extract only a structured subset of available metadata, PDF Forensics aims to extract *all* relevant file system and application metadata in their native format. These extracted data are then collected into a structured HTML-formatted report.

## Input 

### Modes

The utility has two input modes, which are defined as the subcommands:

* `single` This subcommand engages the single input mode of the utility, which handles one PDF file at a time.
* `batch` This subcommand engages the batch input mode of the utility, which handles a CSV file containing any number of input parameters.

### General Arguments

There are two optional arguments for the utility, that can be used irrespective of the mode:

* `-h, --help` This argument will print the help documentation to the command prompt.
* `-q, --quiet` This argument suppresses the utility's title information from being printed to the command line.

### Single Mode Arguments

There are three specific arguments within the `single` subcommand mode, two of which are required:

* `-i, --input` This argument defines the PDF that will be analyzed. It can be given either as a relative or absolute path. This is a required argument.
* `-o, --output` This argument indicates that output will be written to an external file. If followed by a value, it will be interpreted as the name of a new file which the utility will create (provided that there is at least one validly structured PDF file). If no value is provided, the default output value, `output.html`, will be used. As with the input, either relative or absolute paths can be used to define this. This is also a required argument.
* `-p, --password` This argument defines a password that will be used in decryption attempts of the PDF files input into the utility. If no value is provided, the default value, `''`, will be used. This option requires the prior installation of the [QPDF](http://qpdf.sourceforge.net) utility in order to decrypt encrypted files. This is an optional argument.

### Batch Mode Arguments

There is one specific argument within the `batch` subcommand mode, which is required:

* `-f, --file` This argument defines the CSV file that will be parsed for input parameters. It can be given either as a relative or absolute path.

### Batch Mode Input Parameters

The CSV file that is used in batch mode must adhere to the following structure:

* Each row must have either `2` or `3` columns. Any input files with rows containing column counts outside of these parameters will be unreadable by the utility.
* The first column must contain the paths of the PDF files to be read by the utility. As with the `single` mode, these paths can be relative or absolute.
* The second column must contain the specific paths used to create the output file for each input PDF. These output files must be distinct, or output results may be overwritten when new PDF files are analyzed. These paths can be relative or absolute.
* The third column optionally contains password values used to decrypt the PDF files if it is necessary and the password is known.

## Usage

The utility is run from the command line with this general structure:

```shell
pdf-metadata.py [-h] [-q] {single,batch} ...
```

The general CLI structure for `single` mode is:

```shell
pdf-metadata.py single [-h] -i INPUT_NAME -o OUTPUT_NAME [-p PASSWORD]
```

And, the general CLI structure for `batch` mode is:

```shell
pdf-metadata.py batch [-h] -f INPUT_FILE
```

An example of the utility being used to extract a single encrypted file's metadata might be:

```shell
pdf-metadata.py single -i file.pdf -o ../../documents/file_meta.html -p password123
```

An example of the utility being used to analyze a batch of PDF files might be:

```shell
pdf-metadata.py batch -f file.csv
```

For an example of the utility in use, see the following screenshots:

![cli-screenshot-single](example/single/screenshot.png)

![cli-screenshot-batch](example/batch/screenshot.png)

## Output

The utility generates an HTML formatted report, which contains the following contents:

* File System Metadata
    * File Statistics
    * File Hashes
* Application Metadata
    * PDF Version
    * Encryption Status
    * Document Information DIctionary
    * XMP Metadata
    
### File System Metadata

There are two categories of file system metadata included in the report: (1) File Statistics, and (2) File Hashes.

#### File Statistics

There are five statistics about the file included in the report:

* Absolute Path
* Human Readable Size
* Most Recent Access Timestamp
* Most Recent Modification Timestamp
* Most Recent Change Timestamp

#### FIle Hashes

The report includes hash digests of the file in each of the following algorithms:

* MD5
* SHA1
* SHA224
* SHA256
* SHA384
* SHA512

### Application Metadata

There are four categories of application metadata included in the report: (1) PDF Version, (2) Encryption Status, (3) Document Information Dictionary, and (4) XMP Metadata.
#### PDF Version

The PDF version is determined by extracting the magic number from the PDF's binary stream. An example of this magic number would be:

```
%PDF-1.4
```

#### Encryption Status

This section will determine if the PDF is encrypted. This is done by locating an encryption reference in the binary. An example of this would be:

```postscript
/Encrypt 3 0 R
```

If the file is encrypted, a `QPDF` command will be used to attempt decryption. If no password is provided, the utility will use the default null password.

#### Document Information Dictionary

The report will include any references to document information dictionaries. An example of this reference would be:

```postscript
/Info 280 0 R
```

Based on the located references, any matching document information dictionary objects will also be extracted. An example of this object would be:

```postscript
280 0 obj
<</CreationDate(D:20160703161548-04'00')/Creator(Adobe InDesign CS6 \(Windows\))/ModDate(D:20160703162645-04'00')/Producer(Adobe PDF Library 10.0.1)/Trapped/False>>
endobj
```

#### XMP Metadata

SImilar to the handling of the `/Info` references, the utility will also locate any XMP metadata references. An example of this reference would be:

```postscript
/Metadata 279 0 R
```

Then, any located XMP metadata references are used to locate matching objects. An example of an XMP metadata object would be:

```xml
279 0 obj
<</Length 4718/Subtype/XML/Type/Metadata>>stream
<?xpacket begin="ï»¿" id="W5M0MpCehiHzreSzNTczkc9d"?>
<x:xmpmeta xmlns:x="adobe:ns:meta/" x:xmptk="Adobe XMP Core 5.2-c001 63.139439, 2010/09/27-13:37:26        ">
   <rdf:RDF xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
      <rdf:Description rdf:about=""
            xmlns:xmp="http://ns.adobe.com/xap/1.0/">
         <xmp:CreateDate>2016-07-03T16:15:48-04:00</xmp:CreateDate>
         <xmp:MetadataDate>2016-07-03T16:26:45-04:00</xmp:MetadataDate>
         <xmp:ModifyDate>2016-07-03T16:26:45-04:00</xmp:ModifyDate>
         <xmp:CreatorTool>Adobe InDesign CS6 (Windows)</xmp:CreatorTool>
      </rdf:Description>
      <rdf:Description rdf:about=""
            xmlns:xmpMM="http://ns.adobe.com/xap/1.0/mm/"
            xmlns:stRef="http://ns.adobe.com/xap/1.0/sType/ResourceRef#"
            xmlns:stEvt="http://ns.adobe.com/xap/1.0/sType/ResourceEvent#">
         <xmpMM:InstanceID>uuid:672a6a97-7e47-48ed-95f4-443d58650879</xmpMM:InstanceID>
         <xmpMM:OriginalDocumentID>adobe:docid:indd:e4e60777-cdc1-11e1-b3bc-8c95952e2dbb</xmpMM:OriginalDocumentID>
         <xmpMM:DocumentID>xmp.id:72EF63A05A41E611855AE2067B83E76F</xmpMM:DocumentID>
         <xmpMM:RenditionClass>proof:pdf</xmpMM:RenditionClass>
         <xmpMM:DerivedFrom rdf:parseType="Resource">
            <stRef:instanceID>xmp.iid:88A075A33041E611AEB7CC110EFF0350</stRef:instanceID>
            <stRef:documentID>xmp.did:06802DBB9026E61198A2B5CA8073EED3</stRef:documentID>
            <stRef:originalDocumentID>adobe:docid:indd:e4e60777-cdc1-11e1-b3bc-8c95952e2dbb</stRef:originalDocumentID>
            <stRef:renditionClass>default</stRef:renditionClass>
         </xmpMM:DerivedFrom>
         <xmpMM:History>
            <rdf:Seq>
               <rdf:li rdf:parseType="Resource">
                  <stEvt:action>converted</stEvt:action>
                  <stEvt:parameters>from application/x-indesign to application/pdf</stEvt:parameters>
                  <stEvt:softwareAgent>Adobe InDesign CS6 (Windows)</stEvt:softwareAgent>
                  <stEvt:changed>/</stEvt:changed>
                  <stEvt:when>2016-07-03T16:15:48-04:00</stEvt:when>
               </rdf:li>
            </rdf:Seq>
         </xmpMM:History>
      </rdf:Description>
      <rdf:Description rdf:about=""
            xmlns:dc="http://purl.org/dc/elements/1.1/">
         <dc:format>application/pdf</dc:format>
      </rdf:Description>
      <rdf:Description rdf:about=""
            xmlns:pdf="http://ns.adobe.com/pdf/1.3/">
         <pdf:Producer>Adobe PDF Library 10.0.1</pdf:Producer>
         <pdf:Trapped>False</pdf:Trapped>
      </rdf:Description>
   </rdf:RDF>
</x:xmpmeta>
<?xpacket end="w"?>
endstream
endobj
```

### Report Example

For an example of the report, see either the [output.html](https://gitlab.com/nxl4/pdf-metadata/blob/master/example/single/output.html) file in the repository's `example` subdirectory, or the below static image<sup>[1](#myfootnote1)</sup> of the same HTML document:

![static-image](example/single/output.png)

## Issues

Please report any issues to the [Issue Tracker](https://gitlab.com/nxl4/pdf-metadata/issues/new) for this repository. 

## Author

nxl4: [nxl4@protonmail.com](nxl4@protonmail.com)

## License

[GNU General Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html)

## Notes
<a name="myfootnote1">1</a>: This static image was produced using the following `wkhtmltoimage` command:

```bash
wkhtmltoimage --quality 100 file:///home/nxl4/Gitlab/pdf-metadata/example/single/output.html /home/nxl4/Gitlab/pdf-metadata/example/single/output.png
```
