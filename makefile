PREFIX ?= /usr

all:
	@echo Run \'make install\' to install pdf-metadata.

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p pdf-metadata.py $(DESTDIR)$(PREFIX)/bin/pdf-metadata
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/pdf-metadata

uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/pdf-metadata
